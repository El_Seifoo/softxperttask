package com.soft_expert_task.cars;

import android.app.Application;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.soft_expert_task.MySingleton;
import com.soft_expert_task.R;
import com.soft_expert_task.models.ApiResponse;
import com.soft_expert_task.models.Car;
import com.soft_expert_task.models.Error;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarsViewModel extends AndroidViewModel {
    private ViewListener viewListener;/* communicator betn. view model and its view */
    private ObservableField<Integer> progress;/* flag to know the status of progress dialog if loading or not */
    private ObservableField<Integer> loadMoreProgress;/* flag to know the status of load more progress dialog if loading or not */
    private ObservableField<Integer> emptyListTextView;/* flag to know if the cars list is empty or not */
    private ObservableField<String> errorMessage;/* error message of cars request */
    private ObservableField<Integer> errorView;/* flag to know if error view which contains error message is visible or not */

    public CarsViewModel(@NonNull Application application) {
        super(application);
        progress = new ObservableField<>(View.GONE);
        loadMoreProgress = new ObservableField<>(View.GONE);
        emptyListTextView = new ObservableField<>(View.GONE);
        errorView = new ObservableField<>(View.GONE);
        errorMessage = new ObservableField<>("");
    }

    /*
        call model fn to fetch all available cars
     */
    private MutableLiveData<List<Car>> carsMutableLiveData = new MutableLiveData<>();
    private int page = 0;

    public void resetPage() {
        page = 0;
        carsMutableLiveData.setValue(null);
    }

    protected MutableLiveData<List<Car>> requestCars() {
        if (carsMutableLiveData.getValue() == null) {
            page++;
            loadCars();
        } else if (carsMutableLiveData.getValue() != null && this.page > 0) {
            page++;
            loadCars();
        }

        return carsMutableLiveData;
    }

    private void loadCars() {
        if (page == 1)
            setProgress(View.VISIBLE);
        else setLoadMoreProgress(View.VISIBLE);

        Call<ApiResponse<List<Car>>> call = MySingleton.getInstance().createService().fetchCountries(page);
        call.enqueue(new Callback<ApiResponse<List<Car>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<Car>>> call, Response<ApiResponse<List<Car>>> response) {
                viewListener.setRefreshing();
                if (page == 1)
                    setProgress(View.GONE);
                else setLoadMoreProgress(View.GONE);
                int respCode = response.code();
                if (respCode == 200) {
                    ApiResponse<List<Car>> apiResponse = response.body();
                    if (apiResponse.getStatus() == 1) {//status true
                        setEmptyListTextView(View.GONE);
                        carsMutableLiveData.setValue(apiResponse.getData());
                    } else if (apiResponse.getStatus() == 0) {
                        page--;
                        Error error = apiResponse.getError();
                        if (error.getCode() == 1) {
                            if (page == 1) {
                                setEmptyListTextView(View.VISIBLE);// there are no cars fetched
                                carsMutableLiveData.setValue(new ArrayList<Car>());
                            } else {
                                setEmptyListTextView(View.GONE);
                                viewListener.showToastMessage(error.getMessage());
                            }
                        } else {
                            if (page == 1)
                                showResponseMessage(getApplication().getString(R.string.error_fetching_data));
                            else
                                viewListener.showToastMessage(getApplication().getString(R.string.error_fetching_data));
                        }
                    }
                } else {
                    page--;
                    if (page == 1)
                        showResponseMessage(getApplication().getString(R.string.error_fetching_data));
                    else
                        viewListener.showToastMessage(getApplication().getString(R.string.error_fetching_data));
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<List<Car>>> call, Throwable t) {
                viewListener.setRefreshing();
                page--;
                onFailureHandler(t);
            }
        });
    }

    /*
        show error view and show the error message
     */
    public void showResponseMessage(String message) {
        carsMutableLiveData.setValue(null);// when request fail make it equals null to be able to request cars again
        setErrorView(View.VISIBLE);
        setErrorMessage(message);
    }

    /*
        decide what the error is and show to user
     */
    public void onFailureHandler(Throwable t) {
        if (page == 1) {
            setProgress(View.GONE);
            carsMutableLiveData.setValue(null);// when request fail make it equals null to be able to request countries again
            showResponseMessage(t instanceof IOException ?
                    getApplication().getString(R.string.no_internet_connection) :
                    getApplication().getString(R.string.error_fetching_data));
        } else {
            setLoadMoreProgress(View.GONE);
            viewListener.showToastMessage(getApplication().getString(R.string.error_fetching_data));
        }
    }

    /*
        handle endless scrolling
     */
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        if (dy > 0) {
            int pastVisibleItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
            int total = recyclerView.getAdapter().getItemCount();
            if (progress.get() == View.GONE || loadMoreProgress.get() == View.GONE) {
                if (pastVisibleItem >= total - 1) {
                    this.requestCars();
                }
            }
        }
    }


    // Setters & Getters --- start --- \\
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<Integer> getLoadMoreProgress() {
        return loadMoreProgress;
    }

    public ObservableField<Integer> getEmptyListTextView() {
        return emptyListTextView;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    protected void setErrorMessage(String message) {
        errorMessage.set(message);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    protected void setErrorView(int errorViewStatus) {
        this.errorView.set(errorViewStatus);
    }

    public void setProgress(int progress) {
        this.progress.set(progress);
    }

    public void setLoadMoreProgress(int progress) {
        this.loadMoreProgress.set(progress);
    }

    public void setEmptyListTextView(int empty) {
        this.emptyListTextView.set(empty);
    }

    // Setters & Getters --- end --- \\


    protected interface ViewListener {
        void showToastMessage(String message);

        void setRefreshing();
    }

}
