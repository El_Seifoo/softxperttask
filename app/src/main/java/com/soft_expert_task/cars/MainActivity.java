package com.soft_expert_task.cars;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.soft_expert_task.R;
import com.soft_expert_task.cars.CarsAdapter;
import com.soft_expert_task.cars.CarsPresenter;
import com.soft_expert_task.cars.CarsViewModel;
import com.soft_expert_task.databinding.ActivityMainBinding;
import com.soft_expert_task.models.Car;

import java.util.List;

public class MainActivity extends AppCompatActivity implements CarsPresenter, CarsViewModel.ViewListener {
    private ActivityMainBinding binding;
    private SwipeRefreshLayout swipeRefreshLayout;
    private CarsViewModel viewModel;
    private RecyclerView carsList;
    private CarsAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        viewModel = new ViewModelProvider(this).get(CarsViewModel.class);
        viewModel.setViewListener(this);
        binding.setViewModel(viewModel);
        binding.setPresenter(this);

        carsList = binding.carsList;
        carsList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        adapter = new CarsAdapter();
        carsList.setAdapter(adapter);


        carsList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                viewModel.onScrolled(recyclerView, dx, dy);
            }
        });

        viewModel.requestCars().observe(this,
                new Observer<List<Car>>() {
                    @Override
                    public void onChanged(List<Car> cars) {
                        adapter.setCars(cars);
                    }
                });

        swipeRefreshLayout = binding.swipeRefreshLayout;
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                adapter.resetList();
                viewModel.resetPage();
                viewModel.requestCars();
            }
        });
    }

    @Override
    public void onRetryClicked() {
        viewModel.requestCars();
    }


    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setRefreshing() {
        swipeRefreshLayout.setRefreshing(false);
    }
}
