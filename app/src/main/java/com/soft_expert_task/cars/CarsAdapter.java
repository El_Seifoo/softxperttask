package com.soft_expert_task.cars;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.soft_expert_task.R;
import com.soft_expert_task.databinding.CarsListItemBinding;
import com.soft_expert_task.models.Car;

import java.util.List;

public class CarsAdapter extends RecyclerView.Adapter<CarsAdapter.Holder> {
    private List<Car> cars;

    public void setCars(List<Car> cars) {
        if (this.cars == null)
            this.cars = cars;
        else this.cars.addAll(cars);
        notifyDataSetChanged();
    }

    public void resetList() {
        cars = null;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CarsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder((CarsListItemBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.cars_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CarsAdapter.Holder holder, int position) {
        holder.binding.setCar(cars.get(position));
    }

    @Override
    public int getItemCount() {
        return cars != null ? cars.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder {
        private CarsListItemBinding binding;

        public Holder(@NonNull CarsListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
