package com.soft_expert_task;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

public class BindingUtils {

    @BindingAdapter("android:load_image")
    public static void setSrc(ImageView imageView, String path) {
        Glide.with(imageView.getContext())
                .load(path)
                .thumbnail(0.5f)
                .error(R.mipmap.ic_launcher)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);
    }

    @BindingAdapter("android:set_text")
    public static void setConstYear(TextView view, String data) {
        if (data == null)
            view.setVisibility(View.GONE);
        else if (data.isEmpty())
            view.setVisibility(View.GONE);
        else view.setText(view.getContext().getString(R.string.construction_year) + ": " + data);
    }

    @BindingAdapter("android:set_is_used")
    public static void setIsUsed(TextView view,boolean isUsed){
        if (isUsed)
            view.setText(view.getContext().getString(R.string.used));
        else view.setText(view.getContext().getString(R.string.new_));
    }
}
