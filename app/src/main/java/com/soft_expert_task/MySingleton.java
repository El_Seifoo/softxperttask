package com.soft_expert_task;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MySingleton {
    private static MySingleton mInstance;
    private Retrofit retrofit;

    private MySingleton(){
        retrofit = getRetrofit();
    }

    private Retrofit getRetrofit() {
        if (retrofit == null) {
            OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            if (BuildConfig.DEBUG)
                okHttpClient.addInterceptor(loggingInterceptor);

            Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl(URLs.ROOT)
                    .client(okHttpClient.build())
                    .addConverterFactory(GsonConverterFactory.create());
            retrofit = builder.build();
        }

        return retrofit;
    }

    public ApiServiceClient createService() {
        return retrofit.create(ApiServiceClient.class);
    }

    public static synchronized MySingleton getInstance() {
        if (mInstance == null) {
            mInstance = new MySingleton();
        }

        return mInstance;
    }
}
