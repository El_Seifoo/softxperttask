package com.soft_expert_task;

import com.soft_expert_task.models.ApiResponse;
import com.soft_expert_task.models.Car;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiServiceClient {
    @GET(URLs.CARS)
    Call<ApiResponse<List<Car>>> fetchCountries(@Query("page") int page);
}
