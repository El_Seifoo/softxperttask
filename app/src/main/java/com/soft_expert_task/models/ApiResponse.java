package com.soft_expert_task.models;

import com.google.gson.annotations.SerializedName;

public class ApiResponse<S> {
    @SerializedName("error")
    private Error error;
    @SerializedName("status")
    private int status;
    @SerializedName("data")
    private S data;

    public Error getError() {
        return error;
    }

    public int getStatus() {
        return status;
    }

    public S getData() {
        return data;
    }
}
