package com.soft_expert_task.models;

import com.google.gson.annotations.SerializedName;

public class Error {
    @SerializedName("code")
    private int code;
    @SerializedName("message")
    private String message;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
