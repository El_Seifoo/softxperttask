package com.soft_expert_task.models;

import com.google.gson.annotations.SerializedName;

public class Car {
    @SerializedName("id")
    private String id ;
    @SerializedName("brand")
    private String brand;
    @SerializedName("constructionYear")
    private String constYear;
    @SerializedName("isUsed")
    private boolean isUsed;
    @SerializedName("imageUrl")
    private String photo;

    public String getId() {
        return id;
    }

    public String getBrand() {
        return brand;
    }

    public String getConstYear() {
        return constYear;
    }

    public boolean isUsed() {
        return isUsed;
    }

    public String getPhoto() {
        return photo;
    }
}
